var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/', function(req, res, next) {
  let numero = parseInt(req.body.currentNumber);
  let operador = req.body.operator;
  let resultado;
  if(operador == "seno"){
    resultado = Math.sin(numero);
  }else if(operador == "coseno"){
    resultado = Math.cos(numero);
  }else if(operador == "tangente"){
    resultado = Math.tan(numero);
  }
  res.json({resulta: resultado});
});

module.exports = router;
